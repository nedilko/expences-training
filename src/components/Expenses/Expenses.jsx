import { useState } from 'react';
import Card from '../UI/Card';
import ExpensesFilter from './ExpensesFilter';
import ExpensesList from './ExpensesList';
import './Expenses.css';
import ExpensesChart from './ExpensesChart';

function Expenses(props) {
  const [filterValue, setFilterValue] = useState('2021');
  const filteredItems = props.data.filter(
    (item) => item.date.getFullYear() === +filterValue
  );
  const handleOnFilterSelect = (data) => {
    setFilterValue(data);
  };
  return (
    <div>
      <Card className="expenses">
        <ExpensesFilter
          filterValue={filterValue}
          onFilterSelect={handleOnFilterSelect}
        />
        <ExpensesChart expenses={filteredItems}/>
        <ExpensesList items={filteredItems} />
      </Card>
    </div>
  );
}

export default Expenses;
