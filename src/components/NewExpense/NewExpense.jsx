import React, { useState } from 'react';

import ExpenseForm from './ExpenseForm';
import './NewExpense.css';

const NewExpense = (props) => {
  const [isEditing, setIsEditing] = useState(false);
  const onSaveExpsenseData = (data) => {
    props.onSaveExpsenseData({
      id: new Date(),
      ...data,
    });
    setIsEditing(false);
  };

  const startEditingHandler = () => {
    setIsEditing(true);
  };
  const stopEditingHandler = () => {
    setIsEditing(false);
  };

  const newExpenseContent = isEditing
    ? <ExpenseForm onSaveExpsenseData={onSaveExpsenseData} onCancel={stopEditingHandler} />
    : <button onClick={startEditingHandler}>Add New Expense</button>

  return (
    <div className="new-expense">
      {newExpenseContent}
    </div>
  );
};

export default NewExpense;
